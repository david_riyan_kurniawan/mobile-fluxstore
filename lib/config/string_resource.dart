// ignore_for_file: constant_identifier_names

class StringResources {
  //! LOGIN SCREEN
  static const String TEXT_SHOPPING_NOW = "Shopping now";
  static const String TEXT_GET_STARTED = "Get Started";
  static const String TEXT_THE_HOME_FOR_A_FASHIONISTA =
      "The home for a fashionista";
  static const String TEXT_WELCOME_TO_FLUXSTORE = "Welcome to Fluxstore!";

  //! SIGNUP SCREEN
  static const String TEXT_BUAT_AKUN_ANDA = "Buat\nakun anda";
  static const String TEXT_MASUKKAN_NAMA_ANDA = "Masukkan nama anda";
  static const String TEXT_ALAMAT_EMAIL = "Alamat email";
  static const String TEXT_KONFIRMASI_PASSWORD = "Konfirmasi password";
  static const String TEXT_DAFTAR_DENGAN = "Atau daftar dengan";

  //! DASHBOARD SCREEN
  static const String TEXT_FLUXSTORE = "Fluxstore";
  static const String TEXT_FEATURE_PRODUK = "Produk";
  static const String TEXT_SHOW_ALL = "Show all";
}
