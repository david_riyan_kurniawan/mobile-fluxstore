import 'package:flutter/material.dart';
import 'package:fluxstore/features/registrasi/presentation/pages/signup_screen.dart';
import 'package:fluxstore/utils/text_style.dart';
import 'package:lottie/lottie.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 3), () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => const SignUpScreen(),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: size.height * 0.3,
              child: Lottie.asset('assets/lotties/splash-animation.json'),
            ),
            Text(
              'Fluxstore',
              style: brownTextStyle.copyWith(
                fontSize: 25,
                fontWeight: bold,
              ),
            ),
            const SizedBox(height: 15),
            Text(
              'Solusi Terbaik Bagi Anda',
              textAlign: TextAlign.center,
              style: brownTextStyle.copyWith(
                fontSize: 14,
                fontWeight: regular,
              ),
            ),
            Text(
              'Yang ingin Berbelanja Kebutuhan Fashion',
              textAlign: TextAlign.center,
              style: brownTextStyle.copyWith(
                fontSize: 14,
                fontWeight: regular,
              ),
            )
          ],
        ),
      ),
    );
  }
}
