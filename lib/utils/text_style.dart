import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'colors.dart';

TextStyle whiteTextStyle = GoogleFonts.ptSans(color: AppColors.whiteColor);
TextStyle blackTextStyle = GoogleFonts.ptSans(color: AppColors.blackColor);
TextStyle darkGreyTextStyle =
    GoogleFonts.ptSans(color: AppColors.darkgreyColor);
TextStyle brownTextStyle = GoogleFonts.ptSans(color: AppColors.brownColor);
TextStyle greyTextStyle = GoogleFonts.ptSans(color: AppColors.greySecondColor);

//font weight
FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extraBold = FontWeight.w800;
FontWeight thick = FontWeight.w900;
