import 'package:flutter/material.dart';

class AppColors {
  static const Color whiteColor = Color(0xFFFFFFFF);
  static const Color greyColor = Color(0xFFE7E8E9);
  static const Color blackColor = Color(0xFF000000);
  static const Color darkgreyColor = Color(0xFF464447);
  static const Color darkBrownColor = Color(0xFF2D201C);
  static const Color brownColor = Color(0xFF3A2C27);
  static const Color greySecondColor = Color(0xFF9D9D9D);
  static const Color greyThirdColor = Color(0xFFF3F3F3);
  static const Color greyFourthColor = Color(0xFFE2E2E2);
}
