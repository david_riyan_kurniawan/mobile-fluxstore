import 'package:flutter/material.dart';
import 'package:fluxstore/config/string_resource.dart';
import 'package:fluxstore/features/registrasi/presentation/pages/signup_screen.dart';
import 'package:fluxstore/features/login/presentation/widgets/page_view_item.dart';
import 'package:fluxstore/utils/text_style.dart';
import 'package:page_transition/page_transition.dart';

class IntroScreen extends StatefulWidget {
  const IntroScreen({super.key});

  @override
  State<IntroScreen> createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  final PageController pageController = PageController();
  int currentScreen = 0;

  @override
  void initState() {
    super.initState();
    pageController.addListener(() {
      setState(() {
        currentScreen = pageController.page!.round();
      });
    });
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: const Color(0xff464447),
        body: SizedBox(
          height: size.height,
          child: Column(
            children: [
              Flexible(
                child: PageView(
                  controller: pageController,
                  children: const [
                    ScreenPertama(),
                    ScreenKedua(),
                    ScreenKetiga(),
                  ],
                ),
              ),
              buildIndicator(),
              SizedBox(height: size.height * 0.03),
              Container(
                height: 53,
                width: 223,
                decoration: BoxDecoration(
                  color: Colors.white24,
                  border: Border.all(color: Colors.white),
                  borderRadius: BorderRadius.circular(29.5),
                ),
                child: Material(
                  borderRadius: BorderRadius.circular(29.5),
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        PageTransition(
                          child: const SignUpScreen(),
                          type: PageTransitionType.rightToLeft,
                        ),
                      );
                    },
                    borderRadius: BorderRadius.circular(29.5),
                    child: Center(
                      child: Text(
                        StringResources.TEXT_SHOPPING_NOW,
                        textAlign: TextAlign.center,
                        style: whiteTextStyle.copyWith(
                          fontSize: 16,
                          fontWeight: bold,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: size.height / 9,
              ),
            ],
          ),
        ));
  }

  Widget buildIndicator() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List<Widget>.generate(3, (int index) {
        return Container(
          width: 6,
          height: 6,
          margin: const EdgeInsets.symmetric(horizontal: 5),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white),
            shape: BoxShape.circle,
            color:
                currentScreen == index ? Colors.white : const Color(0xff464447),
          ),
        );
      }),
    );
  }
}
