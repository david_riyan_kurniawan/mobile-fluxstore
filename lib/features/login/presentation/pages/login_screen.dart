import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluxstore/features/registrasi/presentation/pages/signup_screen.dart';
import 'package:fluxstore/utils/colors.dart';
import 'package:fluxstore/utils/text_style.dart';
import 'package:page_transition/page_transition.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 25, top: 18),
            child: Text(
              'Masuk ke\nakun Anda',
              style: blackTextStyle.copyWith(
                fontSize: 24,
                fontWeight: bold,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 25),
            child: Column(
              children: [
                SizedBox(height: size.height * 0.05),
                SizedBox(
                  child: TextFormField(
                    decoration: InputDecoration(
                      enabledBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.darkgreyColor,
                        ),
                      ),
                      disabledBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.darkgreyColor,
                        ),
                      ),
                      focusedBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.darkgreyColor,
                        ),
                      ),
                      hintText: 'Alamat email',
                      hintStyle: darkGreyTextStyle.copyWith(
                        fontSize: 14,
                        fontWeight: regular,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: size.height * 0.04),
                SizedBox(
                  child: TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(
                      enabledBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.darkgreyColor,
                        ),
                      ),
                      disabledBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.darkgreyColor,
                        ),
                      ),
                      focusedBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.darkgreyColor,
                        ),
                      ),
                      hintText: 'Password',
                      hintStyle: darkGreyTextStyle.copyWith(
                        fontSize: 14,
                        fontWeight: regular,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'Lupa Password?',
                    style: darkGreyTextStyle.copyWith(
                      fontSize: 12,
                      fontWeight: regular,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Center(
            child: Column(
              children: [
                SizedBox(
                  height: size.height * 0.065,
                ),
                Container(
                  height: size.height * 0.06,
                  width: size.width * 0.3,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(26.5),
                    color: AppColors.darkBrownColor,
                  ),
                  child: Center(
                    child: Text(
                      'LOG IN',
                      style: whiteTextStyle.copyWith(
                        fontWeight: bold,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 25),
                Text(
                  'Atau log in dengan',
                  style: darkGreyTextStyle.copyWith(
                    fontSize: 12,
                    fontWeight: regular,
                  ),
                ),
                const SizedBox(height: 25),
                SizedBox(
                  width: size.width * 0.45,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(
                        height: 42,
                        width: 42,
                        child: SvgPicture.asset('assets/icons/apple_icon.svg'),
                      ),
                      SizedBox(
                        height: 42,
                        width: 42,
                        child: SvgPicture.asset('assets/icons/google_icon.svg'),
                      ),
                      SizedBox(
                        height: 42,
                        width: 42,
                        child:
                            SvgPicture.asset('assets/icons/facebook_icon.svg'),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 25),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Belum memiliki akun? ',
                      style: blackTextStyle.copyWith(
                        fontSize: 14,
                        fontWeight: medium,
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          PageTransition(
                            child: const SignUpScreen(),
                            type: PageTransitionType.leftToRight,
                          ),
                        );
                      },
                      child: Text(
                        'Sign Up',
                        style: blackTextStyle.copyWith(
                          decoration: TextDecoration.underline,
                          fontSize: 14,
                          fontWeight: medium,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
