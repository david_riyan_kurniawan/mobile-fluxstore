import 'package:flutter/material.dart';
import 'package:fluxstore/utils/colors.dart';
import 'package:fluxstore/utils/text_style.dart';
import 'package:google_fonts/google_fonts.dart';

class ScreenPertama extends StatelessWidget {
  const ScreenPertama({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: const Color(0xff464447),
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                height: size.height / 1.9,
                width: double.infinity,
                color: Colors.white,
              ),
              Expanded(
                child: Container(
                  width: double.infinity,
                  color: const Color(0xff464447),
                ),
              ),
            ],
          ),
          Center(
            child: Padding(
              padding: EdgeInsets.only(top: size.height / 10),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text(
                        'Temukan sesuatu yang baru',
                        style: GoogleFonts.ptSans(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    const SizedBox(height: 25),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text(
                        'Pendatang baru yang spesial hanya untuk Anda',
                        style: GoogleFonts.ptSans(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    const SizedBox(height: 24),
                    Container(
                      height: size.height / 2.3,
                      width: size.width / 1.4,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: AppColors.greyColor,
                      ),
                      child: Image.asset(
                        'assets/images/image_intro_pertama.png',
                        fit: BoxFit.contain,
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class ScreenKedua extends StatelessWidget {
  const ScreenKedua({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColors.darkgreyColor,
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                height: size.height / 1.9,
                width: double.infinity,
                color: AppColors.whiteColor,
              ),
              Expanded(
                child: Container(
                  width: double.infinity,
                  color: AppColors.darkgreyColor,
                ),
              ),
            ],
          ),
          Center(
            child: Padding(
              padding: EdgeInsets.only(top: size.height / 10),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text(
                        'Perbarui trend pakaian',
                        style: blackTextStyle.copyWith(
                          fontSize: 20,
                          fontWeight: bold,
                        ),
                      ),
                    ),
                    const SizedBox(height: 25),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text(
                        'Merek favorit dan tren terpanas',
                        style: blackTextStyle.copyWith(
                          fontSize: 14,
                          fontWeight: semiBold,
                        ),
                      ),
                    ),
                    const SizedBox(height: 24),
                    Container(
                      height: size.height / 2.3,
                      width: size.width / 1.4,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: AppColors.greyColor,
                      ),
                      child: Image.asset(
                        'assets/images/image_intro_kedua.png',
                        fit: BoxFit.contain,
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class ScreenKetiga extends StatelessWidget {
  const ScreenKetiga({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColors.darkgreyColor,
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                height: size.height / 1.9,
                width: double.infinity,
                color: Colors.white,
              ),
              Expanded(
                child: Container(
                  width: double.infinity,
                  color: AppColors.darkgreyColor,
                ),
              ),
            ],
          ),
          Center(
            child: Padding(
              padding: EdgeInsets.only(top: size.height / 10),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text(
                        'Jelajahi gaya Anda yang sesungguhnya',
                        style: blackTextStyle.copyWith(
                          fontSize: 20,
                          fontWeight: bold,
                        ),
                      ),
                    ),
                    const SizedBox(height: 25),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text(
                        'Kami menghadirkan gaya untuk Anda',
                        style: GoogleFonts.ptSans(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    const SizedBox(height: 24),
                    Container(
                      height: size.height / 2.3,
                      width: size.width / 1.4,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: AppColors.greyColor,
                      ),
                      child: Image.asset(
                        'assets/images/image_intro_ketiga.png',
                        fit: BoxFit.contain,
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
