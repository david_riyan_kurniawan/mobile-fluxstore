import 'package:fluxstore/features/registrasi/domain/entities/user.dart';

abstract class UserRepository {
  Future<User> registrasiUser();
}
