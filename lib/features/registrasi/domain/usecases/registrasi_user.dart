//! INI ADALAH FILE KHUSUS UNTUK MELAKUKAN REGISTRASI USER

import 'package:fluxstore/features/registrasi/domain/entities/user.dart';
import 'package:fluxstore/features/registrasi/domain/repositories/user_repository.dart';

class RegistrasiUser {
  final UserRepository userRepository;

  RegistrasiUser({required this.userRepository});

  Future<User> execute() async {
    return await userRepository.registrasiUser();
  }

  //!CONTOH EXECUTE REPOSITORY YANG ADA PARAMETERNYA
  // Future<User> execute(int page) async {
  //   return await userRepository.registrasiUser(page);
  // }
}
