// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';

class User extends Equatable {
  String? email;
  String? password;
  String? name;
  String? confirmPassword;

  User({
    this.email,
    this.password,
    this.name,
    this.confirmPassword,
  });

  @override
  List<Object?> get props => [email, password, name, confirmPassword];
}
