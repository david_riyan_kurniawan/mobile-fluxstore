// ignore_for_file: annotate_overrides, overridden_fields, must_be_immutable

import 'package:fluxstore/features/registrasi/domain/entities/user.dart';

class UserModel extends User {
  String? email;
  String? password;
  String? name;
  String? confirmPassword;

  UserModel({
    this.email,
    this.password,
    this.name,
    this.confirmPassword,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) {
    // Map<String,dynamic> data =json['data'];
    return UserModel(
      //contoh penggunaannya: email: data['email']
      email: json['email'],
      password: json['password'],
      name: json['name'],
      confirmPassword: json['confirmPassword'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'password': password,
      'name': name,
      'confirmPassword': confirmPassword,
    };
  }
}
