part of 'registrasi_bloc.dart';

abstract class RegistrasiEvent extends Equatable {
  const RegistrasiEvent();

  @override
  List<Object> get props => [];
}
