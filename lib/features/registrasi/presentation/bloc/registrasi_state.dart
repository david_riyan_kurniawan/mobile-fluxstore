part of 'registrasi_bloc.dart';

abstract class RegistrasiState extends Equatable {
  const RegistrasiState();  

  @override
  List<Object> get props => [];
}
class RegistrasiInitial extends RegistrasiState {}
