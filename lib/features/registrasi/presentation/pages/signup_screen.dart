import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluxstore/config/string_resource.dart';
import 'package:fluxstore/features/dashboard/presentation/pages/dashboard_screen.dart';
import 'package:fluxstore/features/login/presentation/pages/login_screen.dart';
import 'package:fluxstore/utils/colors.dart';
import 'package:fluxstore/utils/text_style.dart';
import 'package:page_transition/page_transition.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: ListView(
        physics: const NeverScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        children: [
          Container(
            margin:
                EdgeInsets.only(top: size.height * 0.02, right: 20, left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  StringResources.TEXT_BUAT_AKUN_ANDA,
                  style: blackTextStyle.copyWith(
                    fontSize: 24,
                    fontWeight: bold,
                  ),
                ),
                SizedBox(height: size.height * 0.04),
                SizedBox(
                  height: size.height * 0.06,
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: StringResources.TEXT_MASUKKAN_NAMA_ANDA,
                      hintStyle: darkGreyTextStyle.copyWith(
                        fontSize: 14,
                      ),
                      border: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.blackColor,
                        ),
                      ),
                      focusedBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.blackColor,
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 15),
                SizedBox(
                  height: size.height * 0.06,
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: StringResources.TEXT_ALAMAT_EMAIL,
                      hintStyle: darkGreyTextStyle.copyWith(fontSize: 14),
                      border: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.blackColor,
                        ),
                      ),
                      focusedBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.blackColor,
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 15),
                SizedBox(
                  height: size.height * 0.06,
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: 'Password',
                      hintStyle: darkGreyTextStyle.copyWith(fontSize: 14),
                      border: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.blackColor,
                        ),
                      ),
                      focusedBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.blackColor,
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 15),
                SizedBox(
                  height: size.height * 0.06,
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: StringResources.TEXT_KONFIRMASI_PASSWORD,
                      hintStyle: darkGreyTextStyle.copyWith(fontSize: 14),
                      border: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.blackColor,
                        ),
                      ),
                      focusedBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.blackColor,
                        ),
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Column(
                    children: [
                      SizedBox(
                        height: size.height * 0.065,
                      ),
                      Container(
                        height: size.height * 0.06,
                        width: size.width * 0.3,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26.5),
                          color: AppColors.darkBrownColor,
                        ),
                        child: InkWell(
                          onTap: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return const DashboardScreen();
                                },
                              ),
                            );
                          },
                          child: Center(
                            child: Text(
                              'SIGN UP',
                              style: whiteTextStyle.copyWith(
                                fontWeight: bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 25),
                      Text(
                        StringResources.TEXT_DAFTAR_DENGAN,
                        style: darkGreyTextStyle.copyWith(
                          fontSize: 12,
                          fontWeight: regular,
                        ),
                      ),
                      const SizedBox(height: 25),
                      SizedBox(
                        width: size.width * 0.45,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            SizedBox(
                              height: 42,
                              width: 42,
                              child: SvgPicture.asset(
                                  'assets/icons/apple_icon.svg'),
                            ),
                            SizedBox(
                              height: 42,
                              width: 42,
                              child: SvgPicture.asset(
                                  'assets/icons/google_icon.svg'),
                            ),
                            SizedBox(
                              height: 42,
                              width: 42,
                              child: SvgPicture.asset(
                                  'assets/icons/facebook_icon.svg'),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 25),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Sudah memiliki akun? ',
                            style: blackTextStyle.copyWith(
                              fontSize: 14,
                              fontWeight: medium,
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const LoginScreen(),
                                  type: PageTransitionType.rightToLeft,
                                ),
                              );
                            },
                            child: Text(
                              'Log In',
                              style: blackTextStyle.copyWith(
                                decoration: TextDecoration.underline,
                                fontSize: 14,
                                fontWeight: medium,
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
