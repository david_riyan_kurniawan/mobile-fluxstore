// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluxstore/config/string_resource.dart';
import 'package:fluxstore/utils/colors.dart';
import 'package:fluxstore/utils/text_style.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({super.key});

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 4, vsync: this);
    tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {}); // Refresh the UI when tabs are switched
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      key: scaffoldKey,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            scaffoldKey.currentState!.openDrawer();
          },
          icon: SvgPicture.asset('assets/icons/drawer_icon.svg'),
        ),
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.transparent,
        title: Text(
          StringResources.TEXT_FLUXSTORE,
          style: blackTextStyle.copyWith(
            fontSize: 20,
            fontWeight: bold,
          ),
        ),
        actions: [
          Container(
            margin: EdgeInsets.only(right: size.width * 0.05),
            child: SvgPicture.asset(
              'assets/icons/bell_icon.svg',
            ),
          ),
        ],
      ),
      drawer: SizedBox(
        width: size.width * 0.65,
        child: Drawer(
          elevation: 2,
          child: Column(
            children: [
              Container(
                color: Colors.amber,
              )
            ],
          ),
        ),
      ),
      body: DefaultTabController(
        length: 4,
        child: NestedScrollView(
          headerSliverBuilder: (context, innerBoxIsScrolled) => [
            const SliverToBoxAdapter(
              child: SizedBox(),
            ),
            SliverAppBar(
              elevation: 0,
              automaticallyImplyLeading: false,
              toolbarHeight: size.height * 0.08,
              backgroundColor: AppColors.whiteColor,
              pinned: false,
              bottom: PreferredSize(
                preferredSize: const Size.fromHeight(0),
                child: TabBar(
                  controller: tabController,
                  indicator: const BoxDecoration(
                    color: Colors.transparent,
                  ),
                  overlayColor:
                      const MaterialStatePropertyAll(Colors.transparent),
                  tabs: [
                    //! wanita
                    Tab(
                      height: size.height * 0.07,
                      child: tabController.index == 0
                          ? Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: AppColors.brownColor,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(9.0),
                                    child: SvgPicture.asset(
                                      'assets/icons/vector_women_icon.svg',
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 3),
                                Text(
                                  'Wanita',
                                  style: brownTextStyle.copyWith(
                                    fontSize: 10,
                                    fontWeight: regular,
                                  ),
                                )
                              ],
                            )
                          : Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: AppColors.greyThirdColor,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(9.0),
                                    child: SvgPicture.asset(
                                      color: AppColors.greySecondColor,
                                      'assets/icons/vector_women_icon.svg',
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 3),
                                Text(
                                  'Wanita',
                                  style: greyTextStyle.copyWith(
                                    fontSize: 10,
                                    fontWeight: regular,
                                  ),
                                )
                              ],
                            ),
                    ),
                    //! Pria
                    Tab(
                      height: size.height * 0.07,
                      child: tabController.index == 1
                          ? Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: AppColors.brownColor,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(9.0),
                                    child: SvgPicture.asset(
                                      'assets/icons/vector_men_icon.svg',
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 3),
                                Text(
                                  'Pria',
                                  style: brownTextStyle.copyWith(
                                    fontSize: 10,
                                    fontWeight: regular,
                                  ),
                                )
                              ],
                            )
                          : Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: AppColors.greyThirdColor,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(9.0),
                                    child: SvgPicture.asset(
                                      color: AppColors.greySecondColor,
                                      'assets/icons/vector_men_icon.svg',
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 3),
                                Text(
                                  'Pria',
                                  style: greyTextStyle.copyWith(
                                    fontSize: 10,
                                    fontWeight: regular,
                                  ),
                                )
                              ],
                            ),
                    ),
                    //! AKSESORIS
                    Tab(
                      height: size.height * 0.07,
                      child: tabController.index == 2
                          ? Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: AppColors.brownColor,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(9.0),
                                    child: SvgPicture.asset(
                                      'assets/icons/vector_aksesoris_icon.svg',
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 3),
                                Text(
                                  'Aksesoris',
                                  style: brownTextStyle.copyWith(
                                    fontSize: 10,
                                    fontWeight: regular,
                                  ),
                                )
                              ],
                            )
                          : Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: AppColors.greyThirdColor,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(9.0),
                                    child: SvgPicture.asset(
                                      color: AppColors.greySecondColor,
                                      'assets/icons/vector_aksesoris_icon.svg',
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 3),
                                Text(
                                  'Aksesoris',
                                  style: greyTextStyle.copyWith(
                                    fontSize: 10,
                                    fontWeight: regular,
                                  ),
                                )
                              ],
                            ),
                    ),
                    //! Kecantikan
                    Tab(
                      height: size.height * 0.07,
                      child: tabController.index == 3
                          ? Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: AppColors.brownColor,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(9.0),
                                    child: SvgPicture.asset(
                                      'assets/icons/vector_kecantikan_icon.svg',
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 3),
                                Text(
                                  'Kecantikan',
                                  style: brownTextStyle.copyWith(
                                    fontSize: 10,
                                    fontWeight: regular,
                                  ),
                                )
                              ],
                            )
                          : Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: AppColors.greyThirdColor,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(9.0),
                                    child: SvgPicture.asset(
                                      color: AppColors.greySecondColor,
                                      'assets/icons/vector_kecantikan_icon.svg',
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 3),
                                Text(
                                  'Kecantikan',
                                  style: greyTextStyle.copyWith(
                                    fontSize: 10,
                                    fontWeight: regular,
                                  ),
                                )
                              ],
                            ),
                    ),
                  ],
                ),
              ),
            ),
          ],
          body: TabBarView(
            controller: tabController,
            children: [
              ListView(
                children: [
                  Column(
                    children: [
                      SizedBox(height: size.height * 0.05),
                      Center(
                        child: Container(
                          height: size.height * 0.22,
                          width: size.width / 1.1,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: AppColors.greyColor,
                            image: const DecorationImage(
                              image: AssetImage(
                                'assets/images/women_banner.png',
                              ),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 24),
                      Center(
                        child: SizedBox(
                          width: size.width / 1.1,
                          child: Row(
                            children: [
                              Text(
                                StringResources.TEXT_FEATURE_PRODUK,
                                style: blackTextStyle.copyWith(
                                  fontSize: 20,
                                  fontWeight: bold,
                                ),
                              ),
                              const Spacer(),
                              Text(
                                StringResources.TEXT_SHOW_ALL,
                                style: greyTextStyle.copyWith(
                                  fontSize: 13,
                                  fontWeight: regular,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(height: 18),
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 16),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              Column(
                                children: [
                                  Container(
                                    height: size.height * 0.22,
                                    width: size.width * 0.33,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: AppColors.greyColor,
                                      image: const DecorationImage(
                                        image: AssetImage(
                                            'assets/images/image_produk2.png'),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 10),
                                  Text(
                                    'Dress panjang',
                                    style: blackTextStyle.copyWith(
                                      fontSize: 12,
                                      fontWeight: regular,
                                    ),
                                  ),
                                  Text(
                                    'Rp. 45.000',
                                    style: blackTextStyle.copyWith(
                                      fontSize: 14,
                                      fontWeight: bold,
                                    ),
                                  )
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Column(
                                  children: [
                                    Container(
                                      height: size.height * 0.22,
                                      width: size.width * 0.33,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: AppColors.greyColor,
                                        image: const DecorationImage(
                                          image: AssetImage(
                                              'assets/images/image_produk1.png'),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(height: 10),
                                    Text(
                                      'Pakaian olahraga',
                                      style: blackTextStyle.copyWith(
                                        fontSize: 12,
                                        fontWeight: regular,
                                      ),
                                    ),
                                    Text(
                                      'Rp. 45.000',
                                      style: blackTextStyle.copyWith(
                                        fontSize: 14,
                                        fontWeight: bold,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Column(
                                  children: [
                                    Container(
                                      height: size.height * 0.22,
                                      width: size.width * 0.33,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: AppColors.greyColor,
                                        image: const DecorationImage(
                                          image: AssetImage(
                                              'assets/images/image_produk.png'),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(height: 10),
                                    Text(
                                      'Dress panjang',
                                      style: blackTextStyle.copyWith(
                                        fontSize: 12,
                                        fontWeight: regular,
                                      ),
                                    ),
                                    Text(
                                      'Rp. 45.000',
                                      style: blackTextStyle.copyWith(
                                        fontSize: 14,
                                        fontWeight: bold,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(height: 25),
                      Container(
                        width: size.width,
                        height: size.height * 0.22,
                        decoration: const BoxDecoration(
                          color: AppColors.greyFourthColor,
                        ),
                        child: Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                top: size.height * 0.04,
                                bottom: size.height * 0.04,
                                left: size.width * 0.08,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'KOLEKSI BARU SAYA',
                                    style: greyTextStyle.copyWith(
                                      fontSize: 16,
                                      fontWeight: light,
                                    ),
                                  ),
                                  Text(
                                    'HANG OUT \n&PESTA',
                                    style: darkGreyTextStyle.copyWith(
                                      fontSize: 20,
                                      fontWeight: regular,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            const Spacer(),
                            Padding(
                              padding: const EdgeInsets.only(right: 15),
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Container(
                                    height: size.height * 0.2,
                                    width: size.width * 0.4,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(80),
                                      color: AppColors.greySecondColor,
                                    ),
                                  ),
                                  Container(
                                    height: size.height * 0.16,
                                    width: size.width * 0.31,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(80),
                                      color: AppColors.darkgreyColor,
                                    ),
                                  ),
                                  SizedBox(
                                    height: size.height,
                                    width: size.width * 0.33,
                                    child: Image.asset(
                                      'assets/images/image_produk3.png',
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: size.height * 0.05),
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Rekomendasi',
                              style: blackTextStyle.copyWith(
                                fontSize: 20,
                                fontWeight: bold,
                              ),
                            ),
                            Text(
                              'Show all',
                              style: greyTextStyle.copyWith(
                                fontSize: 13,
                                fontWeight: regular,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: size.height * 0.05),
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 14),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              Container(
                                height: size.height * 0.085,
                                width: size.width * 0.55,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  border: Border.all(
                                    color: AppColors.greySecondColor,
                                  ),
                                ),
                                child: Row(
                                  children: [
                                    Container(
                                      height: size.height * 0.085,
                                      width: size.width * 0.17,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        color: AppColors.greyColor,
                                        image: const DecorationImage(
                                          image: AssetImage(
                                            'assets/images/image_jaket.png',
                                          ),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 7.0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Jaket',
                                            style: blackTextStyle.copyWith(
                                              fontSize: 12,
                                              fontWeight: regular,
                                            ),
                                          ),
                                          Text(
                                            'Rp. 90.000',
                                            style: blackTextStyle.copyWith(
                                              fontSize: 16,
                                              fontWeight: bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Container(
                                  height: size.height * 0.085,
                                  width: size.width * 0.55,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(
                                      color: AppColors.greySecondColor,
                                    ),
                                  ),
                                  child: Row(
                                    children: [
                                      Container(
                                        height: size.height * 0.085,
                                        width: size.width * 0.17,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          color: AppColors.greyColor,
                                          image: const DecorationImage(
                                            image: AssetImage(
                                              'assets/images/cotton_tshirt.png',
                                            ),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 7.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Cotton T-shirt',
                                              style: blackTextStyle.copyWith(
                                                fontSize: 12,
                                                fontWeight: regular,
                                              ),
                                            ),
                                            Text(
                                              'Rp. 90.000',
                                              style: blackTextStyle.copyWith(
                                                fontSize: 16,
                                                fontWeight: bold,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(height: 25),
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Koleksi Teratas',
                              style: blackTextStyle.copyWith(
                                fontSize: 20,
                                fontWeight: bold,
                              ),
                            ),
                            Text(
                              'Show all',
                              style: greyTextStyle.copyWith(
                                fontSize: 13,
                                fontWeight: regular,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: size.height * 0.03),
                      Container(
                        width: size.width / 1.08,
                        height: size.height * 0.18,
                        decoration: BoxDecoration(
                          color: AppColors.greyFourthColor,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                top: size.height * 0.04,
                                bottom: size.height * 0.04,
                                left: size.width * 0.08,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Diskon 30%',
                                    style: greyTextStyle.copyWith(
                                      fontSize: 14,
                                      fontWeight: light,
                                    ),
                                  ),
                                  Text(
                                    'FOR SLIM \n& BEAUTY',
                                    style: darkGreyTextStyle.copyWith(
                                      fontSize: 20,
                                      fontWeight: regular,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            const Spacer(),
                            Padding(
                              padding: const EdgeInsets.only(right: 15),
                              child: Center(
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(right: 20),
                                      child: Container(
                                        height: size.height * 0.13,
                                        width: size.width * 0.27,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(80),
                                          color: AppColors.greySecondColor,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: size.height,
                                      width: size.width * 0.33,
                                      child: Image.asset(
                                        'assets/images/image_diskon.png',
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      const SizedBox(height: 25),
                      Container(
                        width: size.width / 1.08,
                        height: size.height * 0.26,
                        decoration: BoxDecoration(
                          color: AppColors.greyFourthColor,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                top: size.height * 0.04,
                                bottom: size.height * 0.04,
                                left: size.width * 0.08,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Diskon 30%',
                                    style: greyTextStyle.copyWith(
                                      fontSize: 14,
                                      fontWeight: light,
                                    ),
                                  ),
                                  SizedBox(height: size.height * 0.035),
                                  Text(
                                    'FOR SLIM \n& BEAUTY',
                                    style: darkGreyTextStyle.copyWith(
                                      fontSize: 20,
                                      fontWeight: regular,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            const Spacer(),
                            Padding(
                              padding: const EdgeInsets.only(right: 15),
                              child: Center(
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(right: 20),
                                      child: Container(
                                        height: size.height * 0.12,
                                        width: size.width * 0.24,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(80),
                                          color: AppColors.greySecondColor,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: size.height,
                                      width: size.width * 0.33,
                                      child: Image.asset(
                                        'assets/images/image_musim_panas.png',
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      const SizedBox(height: 25)
                    ],
                  ),
                ],
              ),
              const Center(
                child: Text('data 2'),
              ),
              const Center(
                child: Text('data 3'),
              ),
              const Center(
                child: Text('data 4'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
